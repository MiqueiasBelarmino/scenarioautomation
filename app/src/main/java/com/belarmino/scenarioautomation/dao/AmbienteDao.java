package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.util.Tools;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class AmbienteDao implements Closeable {

    protected static final String tableAmbientes = "Ambientes";
    protected static final String ID = "id";
    protected static final String colNome = "nome";
    protected static final String colImagem = "imagem";
    protected static final String colProjeto = "projeto";


    private DBHelper db;
    private Context context;

    public AmbienteDao(Context context) {
        
        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(Ambiente ambiente) {

        ContentValues values = getContentValues(ambiente);
        return db.getWritableDatabase().insert(tableAmbientes, null, values);
    }

    public long update(Ambiente ambiente) {

        ContentValues contentValues = getContentValues(ambiente);
        return db.getWritableDatabase().update(tableAmbientes, contentValues, "id = ?", new String[]{String.valueOf(ambiente.getId())});

    }

    public List<Ambiente> findAll() {

        List<Ambiente> ambientes = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableAmbientes, null);

        while (cursor.moveToNext())
            ambientes.add(getAmbienteCursor(cursor));
        cursor.close();

        return ambientes;
    }

    public List<Ambiente> findByProjeto(long id) {

        List<Ambiente> ambientes = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableAmbientes + " where projeto =?", new String[]{String.valueOf(id)});

        while (cursor.moveToNext())
            ambientes.add(getAmbienteCursor(cursor));
        cursor.close();

        return ambientes;
    }

    public void delete(Ambiente ambiente) {

        db.getWritableDatabase().delete(tableAmbientes, "id = ?", new String[]{String.valueOf(ambiente.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableAmbientes, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(Ambiente ambiente) {

        ContentValues values = new ContentValues();
        values.put(colNome, ambiente.getNome());
        values.put(colImagem, Tools.getBytes(Tools.decodeToLowResImage(ambiente.getImagem(),100,100)));
        values.put(colProjeto, ambiente.getProjeto().getId());

        return values;
    }

    private Ambiente getAmbienteCursor(Cursor cursor) {

        Ambiente ambiente = new Ambiente();
        ambiente.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        ambiente.setNome(cursor.getString(cursor.getColumnIndex(colNome)));
        ambiente.setImagem(cursor.getBlob(cursor.getColumnIndex(colImagem)));
        ambiente.setProjeto(new ProjetoDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colProjeto)))));

        return ambiente;
    }


    public Ambiente findById(long id) {
        
        Ambiente ambiente = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableAmbientes + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if(cursor != null) {
            cursor.moveToFirst();
            ambiente = getAmbienteCursor(cursor);
        }
        cursor.close();
        return ambiente;
    }

    @Override
    public void close() {
        db.close();
    }

}
