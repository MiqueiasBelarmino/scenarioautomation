package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.DispositivoScenario;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class DispositivoScenarioDao implements Closeable {

    protected static final String tableDispositivoScenario = "DispositivoScenario";
    protected static final String ID = "id";
    protected static final String colTipo = "tipo";
    protected static final String colAmbiente = "ambiente";
    protected static final String colDados = "dadosDispositivo";


    private DBHelper db;
    private Context context;

    public DispositivoScenarioDao(Context context) {

        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(DispositivoScenario dispositivoScenario) {

        ContentValues values = getContentValues(dispositivoScenario);
        return db.getWritableDatabase().insert(tableDispositivoScenario, null, values);
    }

    public long update(DispositivoScenario dispositivoScenario) {

        ContentValues contentValues = getContentValues(dispositivoScenario);
        return db.getWritableDatabase().update(tableDispositivoScenario, contentValues, "id = ?", new String[]{String.valueOf(dispositivoScenario.getId())});

    }

    public List<DispositivoScenario> findAll() {

        List<DispositivoScenario> dispositivoScenarios = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableDispositivoScenario, null);

        while (cursor.moveToNext())
            dispositivoScenarios.add(getDispositivoScenarioCursor(cursor));
        cursor.close();

        return dispositivoScenarios;
    }

    public List<DispositivoScenario> findByAmbiente(long id) {

        List<DispositivoScenario> dispositivoScenarios = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableDispositivoScenario + " where ambiente =?", new String[]{String.valueOf(id)});

        while (cursor.moveToNext())
            dispositivoScenarios.add(getDispositivoScenarioCursor(cursor));
        cursor.close();

        return dispositivoScenarios;
    }

    public void delete(DispositivoScenario dispositivoScenario) {

        db.getWritableDatabase().delete(tableDispositivoScenario, "id = ?", new String[]{String.valueOf(dispositivoScenario.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableDispositivoScenario, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(DispositivoScenario dispositivoScenario) {

        ContentValues values = new ContentValues();
        values.put(colTipo, dispositivoScenario.getTipo().getId());
        values.put(colAmbiente, dispositivoScenario.getAmbiente().getId());
        values.put(colDados, dispositivoScenario.getDadosDispositivo().getId());

        return values;
    }

    private DispositivoScenario getDispositivoScenarioCursor(Cursor cursor) {

        DispositivoScenario dispositivoScenario = new DispositivoScenario();
        dispositivoScenario.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        dispositivoScenario.setTipo(new TiposDispositivoDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colTipo)))));
        dispositivoScenario.setAmbiente(new AmbienteDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colAmbiente)))));
        dispositivoScenario.setDadosDispositivo(new DadosDispositivoDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colDados)))));

        return dispositivoScenario;
    }


    public DispositivoScenario findById(long id) {

        DispositivoScenario dispositivoScenario = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableDispositivoScenario + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            cursor.moveToFirst();
            dispositivoScenario = getDispositivoScenarioCursor(cursor);
        }
        cursor.close();
        return dispositivoScenario;
    }

    @Override
    public void close() {
        db.close();
    }

}
