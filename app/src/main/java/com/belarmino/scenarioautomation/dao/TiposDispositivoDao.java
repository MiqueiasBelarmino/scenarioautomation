package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.TiposDispositivo;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class TiposDispositivoDao implements Closeable {

    protected static final String tableTiposDispositivos = "TiposDispositivoScenario";
    protected static final String ID = "id";
    protected static final String colDescricao = "descricao";


    private DBHelper db;
    private Context context;

    public TiposDispositivoDao(Context context) {

        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(TiposDispositivo tiposDispositivos) {

        ContentValues values = getContentValues(tiposDispositivos);
        return db.getWritableDatabase().insert(tableTiposDispositivos, null, values);
    }

    public long update(TiposDispositivo tiposDispositivos) {

        ContentValues contentValues = getContentValues(tiposDispositivos);
        return db.getWritableDatabase().update(tableTiposDispositivos, contentValues, "id = ?", new String[]{String.valueOf(tiposDispositivos.getId())});

    }

    public List<TiposDispositivo> findAll() {

        List<TiposDispositivo> tiposDispositivos = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableTiposDispositivos, null);

        while (cursor.moveToNext())
            tiposDispositivos.add(getTiposDispositivosCursor(cursor));
        cursor.close();

        return tiposDispositivos;
    }

    public void delete(TiposDispositivo tiposDispositivos) {

        db.getWritableDatabase().delete(tableTiposDispositivos, "id = ?", new String[]{String.valueOf(tiposDispositivos.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableTiposDispositivos, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(TiposDispositivo tiposDispositivos) {

        ContentValues values = new ContentValues();
        values.put(colDescricao, tiposDispositivos.getDescricao());

        return values;
    }

    private TiposDispositivo getTiposDispositivosCursor(Cursor cursor) {

        TiposDispositivo tiposDispositivos = new TiposDispositivo();
        tiposDispositivos.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        tiposDispositivos.setDescricao(cursor.getString(cursor.getColumnIndex(colDescricao)));

        return tiposDispositivos;
    }


    public TiposDispositivo findById(long id) {

        TiposDispositivo tiposDispositivos = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableTiposDispositivos + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            cursor.moveToFirst();
            tiposDispositivos = getTiposDispositivosCursor(cursor);
        }
        cursor.close();
        return tiposDispositivos;
    }

    @Override
    public void close() {
        db.close();
    }

}
