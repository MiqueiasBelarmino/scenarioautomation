package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.DadosDispositivo;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class DadosDispositivoDao implements Closeable {

    protected static final String tableDadosDispositivos = "DadosDispositivo";
    protected static final String ID = "id";
    protected static final String colNome = "nome";


    private DBHelper db;
    private Context context;

    public DadosDispositivoDao(Context context) {

        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(DadosDispositivo dadosDispositivo) {

        ContentValues values = getContentValues(dadosDispositivo);
        return db.getWritableDatabase().insert(tableDadosDispositivos, null, values);
    }

    public long update(DadosDispositivo dadosDispositivo) {

        ContentValues contentValues = getContentValues(dadosDispositivo);
        return db.getWritableDatabase().update(tableDadosDispositivos, contentValues, "id = ?", new String[]{String.valueOf(dadosDispositivo.getId())});

    }

    public List<DadosDispositivo> findAll() {

        List<DadosDispositivo> dadosDispositivo = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableDadosDispositivos, null);

        while (cursor.moveToNext())
            dadosDispositivo.add(getDadosDispositivoCursor(cursor));
        cursor.close();

        return dadosDispositivo;
    }

    public void delete(DadosDispositivo dadosDispositivo) {

        db.getWritableDatabase().delete(tableDadosDispositivos, "id = ?", new String[]{String.valueOf(dadosDispositivo.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableDadosDispositivos, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(DadosDispositivo dadosDispositivo) {

        ContentValues values = new ContentValues();
        values.put(colNome, dadosDispositivo.getNome());

        return values;
    }

    private DadosDispositivo getDadosDispositivoCursor(Cursor cursor) {

        DadosDispositivo dadosDispositivo = new DadosDispositivo();
        dadosDispositivo.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        dadosDispositivo.setNome(cursor.getString(cursor.getColumnIndex(colNome)));

        return dadosDispositivo;
    }


    public DadosDispositivo findById(long id) {

        DadosDispositivo dadosDispositivo = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableDadosDispositivos + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            cursor.moveToFirst();
            dadosDispositivo = getDadosDispositivoCursor(cursor);
        }
        cursor.close();
        return dadosDispositivo;
    }

    @Override
    public void close() {
        db.close();
    }

}
