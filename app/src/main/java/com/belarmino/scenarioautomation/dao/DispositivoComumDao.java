package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.DispositivoComum;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class DispositivoComumDao implements Closeable {

    protected static final String tableDispositivoComum = "DispositivoComum";
    protected static final String ID = "id";
    protected static final String colFabricante = "fabricante";
    protected static final String colAmbiente = "ambiente";
    protected static final String colDados = "dadosDispositivo";


    private DBHelper db;
    private Context context;

    public DispositivoComumDao(Context context) {

        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(DispositivoComum dispositivoComum) {

        ContentValues values = getContentValues(dispositivoComum);
        return db.getWritableDatabase().insert(tableDispositivoComum, null, values);
    }

    public long update(DispositivoComum dispositivoComum) {

        ContentValues contentValues = getContentValues(dispositivoComum);
        return db.getWritableDatabase().update(tableDispositivoComum, contentValues, "id = ?", new String[]{String.valueOf(dispositivoComum.getId())});

    }

    public List<DispositivoComum> findAll() {

        List<DispositivoComum> dispositivoComums = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableDispositivoComum, null);

        while (cursor.moveToNext())
            dispositivoComums.add(getDispositivoComumCursor(cursor));
        cursor.close();

        return dispositivoComums;
    }

    public List<DispositivoComum> findByAmbiente(long id) {

        List<DispositivoComum> dispositivoComums = new ArrayList<>();
        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableDispositivoComum + " where ambiente =?", new String[]{String.valueOf(id)});

        while (cursor.moveToNext())
            dispositivoComums.add(getDispositivoComumCursor(cursor));
        cursor.close();

        return dispositivoComums;
    }

    public void delete(DispositivoComum dispositivoComum) {

        db.getWritableDatabase().delete(tableDispositivoComum, "id = ?", new String[]{String.valueOf(dispositivoComum.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableDispositivoComum, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(DispositivoComum dispositivoComum) {

        ContentValues values = new ContentValues();
        values.put(colFabricante, dispositivoComum.getFabricante());
        values.put(colAmbiente, dispositivoComum.getAmbiente().getId());
        values.put(colDados, dispositivoComum.getDadosDispositivo().getId());

        return values;
    }

    private DispositivoComum getDispositivoComumCursor(Cursor cursor) {

        DispositivoComum dispositivoComum = new DispositivoComum();
        dispositivoComum.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        dispositivoComum.setFabricante(cursor.getString(cursor.getColumnIndex(colFabricante)));
        dispositivoComum.setAmbiente(new AmbienteDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colAmbiente)))));
        dispositivoComum.setDadosDispositivo(new DadosDispositivoDao(context).findById(Long.valueOf(cursor.getString(cursor.getColumnIndex(colDados)))));

        return dispositivoComum;
    }


    public DispositivoComum findById(long id) {

        DispositivoComum dispositivoComum = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableDispositivoComum + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            cursor.moveToFirst();
            dispositivoComum = getDispositivoComumCursor(cursor);
        }
        cursor.close();
        return dispositivoComum;
    }

    @Override
    public void close() {
        db.close();
    }

}
