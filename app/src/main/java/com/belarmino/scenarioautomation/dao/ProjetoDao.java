package com.belarmino.scenarioautomation.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.belarmino.scenarioautomation.model.Projeto;
import com.belarmino.scenarioautomation.util.Tools;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public class ProjetoDao implements Closeable {

    protected static final String tableProjetos = "Projetos";
    protected static final String ID = "id";
    protected static final String colNomeProjeto = "nomeProjeto";
    protected static final String colnomeCliente = "nomeCliente";
    protected static final String colEndereco = "endereco";
    protected static final String colDataAtualizacao = "dataAtualizacao";


    private DBHelper db;
    private Context context;

    public ProjetoDao(Context context) {
        
        this.context = context;
        db = new DBHelper(context);
    }


    public long insert(Projeto projeto) {

        ContentValues values = getContentValues(projeto);
        return db.getWritableDatabase().insert(tableProjetos, null, values);
    }

    public long update(Projeto projeto) {

        ContentValues contentValues = getContentValues(projeto);
        return db.getWritableDatabase().update(tableProjetos, contentValues, "id = ?", new String[]{String.valueOf(projeto.getId())});

    }

    public List<Projeto> findAll() {

        List<Projeto> projetos = new ArrayList<>();

        Cursor cursor = db.getReadableDatabase().rawQuery("select * from " + tableProjetos +" order by date("+ colDataAtualizacao +") DESC", null);

        while (cursor.moveToNext())
            projetos.add(getProjetoCursor(cursor));
        cursor.close();

        return projetos;
    }

    public void delete(Projeto projeto) {

        db.getWritableDatabase().delete(tableProjetos, "id = ?", new String[]{String.valueOf(projeto.getId())});
    }

    public void delete(long id) {

        db.getWritableDatabase().delete(tableProjetos, "id = ?", new String[]{String.valueOf(id)});
    }

    @NonNull
    private ContentValues getContentValues(Projeto projeto) {

        ContentValues values = new ContentValues();
        values.put(colNomeProjeto, projeto.getNomeProjeto());
        values.put(colnomeCliente, projeto.getNomeCliente());
        values.put(colEndereco, projeto.getEndereco());
        values.put(colDataAtualizacao, Tools.dateFormat(projeto.getDataAtualizacao(),"yyyy-MM-dd"));

        return values;
    }

    private Projeto getProjetoCursor(Cursor cursor) {

        Projeto projeto = new Projeto();
        projeto.setId(cursor.getLong(cursor.getColumnIndex(ID)));
        projeto.setNomeProjeto(cursor.getString(cursor.getColumnIndex(colNomeProjeto)));
        projeto.setNomeCliente(cursor.getString(cursor.getColumnIndex(colnomeCliente)));
        projeto.setEndereco(cursor.getString(cursor.getColumnIndex(colEndereco)));
        projeto.setDataAtualizacao(cursor.getString(cursor.getColumnIndex(colDataAtualizacao)));

        return projeto;
    }


    public Projeto findById(long id) {
        
        Projeto projeto = null;
        Cursor cursor = db.getReadableDatabase().rawQuery("Select * from " + tableProjetos + " where " + ID + " = ?", new String[]{String.valueOf(id)});
        if(cursor != null) {
            cursor.moveToFirst();
            projeto = getProjetoCursor(cursor);
        }
        cursor.close();
        return projeto;
    }

    @Override
    public void close() {
        db.close();
    }

}
