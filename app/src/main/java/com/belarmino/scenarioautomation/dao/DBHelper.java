package com.belarmino.scenarioautomation.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String NOME = "ScenarioDB.db";
    private static final int VERSION = 1;

    public DBHelper(Context context) {
        super(context, NOME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlProjeto = "Create table Projetos (" +
                "id integer primary key autoincrement," +
                "nomeProjeto text not null," +
                "nomeCliente text not null," +
                "endereco text not null," +
                "dataAtualizacao datetime not null" +
                ");";

        String sqlAmbiente = "Create table Ambientes (" +
                "id integer primary key autoincrement," +
                "nome text not null," +
                "imagem TEXT," +
                "projeto integer not null, " +
                "FOREIGN KEY(projeto) REFERENCES Projetos(id)" +
                ");";

        String sqlDadosDispositivo = "Create table DadosDispositivo (" +
                "id integer primary key autoincrement," +
                "nome text not null" +
                ");";

        String sqlTiposDispositivoScenario = "Create table TiposDispositivoScenario ( " +
                "id integer primary key autoincrement," +
                "descricao text not null " +
                ") ;";



        String sqlDispositivoComum = "Create table DispositivoComum (" +
                "id integer primary key autoincrement," +
                "fabricante text not null, " +
                "ambiente integer not null, " +
                "dadosDispositivo integer not null, " +
                "FOREIGN KEY(ambiente) REFERENCES Ambientes(id)," +
                "FOREIGN KEY(dadosDispositivo) REFERENCES DadosDispositivo(id)" +
                " );";

        String sqlDispositivoScenario = "Create table DispositivoScenario ("+
                "id integer primary key autoincrement," +
                "tipo integer not null, " +
                "ambiente integer not null, " +
                "dadosDispositivo integer not null, " +
                "FOREIGN KEY(tipo) REFERENCES TiposDispositivoScenario(id)," +
                "FOREIGN KEY(ambiente) REFERENCES Ambientes(id)," +
                "FOREIGN KEY(dadosDispositivo) REFERENCES DadosDispositivo(id)" +
                ");";
        String sqlInsertTiposDispositivosScenario = "" +
                "insert into TiposDispositivoScenario (descricao)" +
                " values(\"Modulo de Iluminação\"),(\"Teclados\"),(\"Modulo de Controle de Cortina\")";

        db.execSQL(sqlProjeto);
        db.execSQL(sqlAmbiente);
        db.execSQL(sqlDadosDispositivo);
        db.execSQL(sqlTiposDispositivoScenario);
        db.execSQL(sqlInsertTiposDispositivosScenario);
        db.execSQL(sqlDispositivoComum);
        db.execSQL(sqlDispositivoScenario);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
