package com.belarmino.scenarioautomation.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.dao.DispositivoComumDao;
import com.belarmino.scenarioautomation.dao.DispositivoScenarioDao;
import com.belarmino.scenarioautomation.model.DispositivoComum;
import com.belarmino.scenarioautomation.model.DispositivoScenario;
import com.belarmino.scenarioautomation.util.Tools;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DispositivoAdapter extends RecyclerView.Adapter {

    private Context context;
    private static final int ITEM_TYPE_SCENARIO = 1;
    private static final int ITEM_TYPE_COMUM = 2;
    private List<Object> objetos = new ArrayList<>();

    public DispositivoAdapter(Context context, List<Object> objetos) {
        this.context = context;
        this.objetos = objetos;
    }

    //@Override
    //public long getItemId(int position) {
    //return position;
    //}

    @Override
    public int getItemViewType(int position) {
        if (objetos.get(position) instanceof DispositivoScenario) {
            return ITEM_TYPE_SCENARIO;
        } else {
            return ITEM_TYPE_COMUM;
        }
    }

    @Override
    public int getItemCount() {
        return objetos.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        if (viewType == ITEM_TYPE_SCENARIO) {
            view = layoutInflater.inflate(R.layout.item_dispositivo_scenario, parent, false);

            return new DispositivoScenarioViewHolder(view);
        } else {
            view = layoutInflater.inflate(R.layout.item_dispositivo_comum, parent, false);

            return new DispositivoComumViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        Object item = objetos.get(position);

        if (viewHolder instanceof DispositivoScenarioViewHolder) {
            DispositivoScenarioViewHolder vHolderScenario = (DispositivoScenarioViewHolder) viewHolder;
            DispositivoScenario dispositivoScenario = (DispositivoScenario) item;
            vHolderScenario.id.setText(String.valueOf(dispositivoScenario.getId()));
            vHolderScenario.dadosId.setText(String.valueOf(dispositivoScenario.getDadosDispositivo().getId()));
            vHolderScenario.tipoId.setText(String.valueOf(dispositivoScenario.getTipo().getId()));
            vHolderScenario.tipo.setText(dispositivoScenario.getTipo().getDescricao());
            vHolderScenario.nome.setText(dispositivoScenario.getDadosDispositivo().getNome());
            vHolderScenario.ambiente.setText(String.valueOf(dispositivoScenario.getAmbiente().getId()));

        } else {
            DispositivoComumViewHolder vHolderComum = (DispositivoComumViewHolder) viewHolder;
            DispositivoComum dispositivoComum = (DispositivoComum) item;
            vHolderComum.id.setText(String.valueOf(dispositivoComum.getId()));
            vHolderComum.dadosId.setText(String.valueOf(dispositivoComum.getDadosDispositivo().getId()));
            vHolderComum.nome.setText(dispositivoComum.getDadosDispositivo().getNome());
            vHolderComum.fabricante.setText(dispositivoComum.getFabricante());
            vHolderComum.ambiente.setText(String.valueOf(dispositivoComum.getAmbiente().getId()));
        }
    }

    class DispositivoScenarioViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @BindView(R.id.item_dispositivo_scenario_id)
        TextView id;
        @BindView(R.id.item_dispositivo_scenario_dados_id)
        TextView dadosId;
        @BindView(R.id.item_dispositivo_scenario_tipo_id)
        TextView tipoId;
        @BindView(R.id.item_dispositivo_scenario_tipo)
        TextView tipo;
        @BindView(R.id.item_dispositivo_scenario_nome)
        TextView nome;
        @BindView(R.id.item_dispositivo_scenario_ambiente_id)
        TextView ambiente;
        @BindView(R.id.item_dispositivo_scenario)
        CardView cardViewDispositivoScenario;

        public DispositivoScenarioViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cardViewDispositivoScenario.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            contextMenu.add(this.getAdapterPosition(), Tools.UPDATE, 0, "Editar");
            contextMenu.add(this.getAdapterPosition(), Tools.DELETE, 1, "Deletar");
        }
    }

    class DispositivoComumViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @BindView(R.id.item_dispositivo_comum_id)
        TextView id;
        @BindView(R.id.item_dispositivo_comum_dados_id)
        TextView dadosId;
        @BindView(R.id.item_dispositivo_comum_nome)
        TextView nome;
        @BindView(R.id.item_dispositivo_comum_fabricante)
        TextView fabricante;
        @BindView(R.id.item_dispositivo_comum_ambiente_id)
        TextView ambiente;
        @BindView(R.id.item_dispositivo_comum)
        CardView cardViewDispositivoComum;

        public DispositivoComumViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cardViewDispositivoComum.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            contextMenu.add(this.getAdapterPosition(), Tools.UPDATE, 0, "Editar");
            contextMenu.add(this.getAdapterPosition(), Tools.DELETE, 1, "Deletar");
        }
    }

    public void deleteDispositivo(Object object) {

        if (object instanceof DispositivoScenario) {
            new DispositivoScenarioDao(context).delete(((DispositivoScenario) object).getId());
        } else if (object instanceof DispositivoComum) {
            new DispositivoComumDao(context).delete(((DispositivoComum) object).getId());
        }
        notifyDataSetChanged();
    }

}
