package com.belarmino.scenarioautomation.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.activity.ListarDispositivoActivity;
import com.belarmino.scenarioautomation.dao.AmbienteDao;
import com.belarmino.scenarioautomation.dao.DispositivoComumDao;
import com.belarmino.scenarioautomation.dao.DispositivoScenarioDao;
import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.util.Tools;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AmbienteAdapter extends RecyclerView.Adapter{

    private Context context;
    private List<Ambiente> ambientes;

    public AmbienteAdapter(Context context, List<Ambiente> ambientes) {
        this.context = context;
        this.ambientes = ambientes;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_ambiente, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder holder = (ViewHolder) viewHolder;
        Ambiente ambiente = ambientes.get(position);

        holder.id.setText(""+ambiente.getId());
        holder.nome.setText(ambiente.getNome());

        byte[] recordImage = ambiente.getImagem();
        Bitmap bitmap = BitmapFactory.decodeByteArray(recordImage, 0, recordImage.length);
        holder.imagem.setImageBitmap(Tools.decodeToLowResImage(recordImage,100,100));
        holder.quantidadeDipositivos.setText("" + qtdDispositivos(ambiente.getId()));
        holder.projeto.setText(""+ambiente.getProjeto().getId());
    }

    private int qtdDispositivos(long id) {
        return new DispositivoComumDao(context).findByAmbiente(id).size() + new DispositivoScenarioDao(context).findByAmbiente(id).size();
    }

    @Override
    public int getItemCount() {
        return ambientes.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @BindView(R.id.item_ambiente_id)
        TextView id;
        @BindView(R.id.item_ambiente_nome)
        TextView nome;
        @BindView(R.id.image_view_ambiente)
        ImageView imagem;
        @BindView(R.id.item_ambiente_projeto)
        TextView projeto;
        @BindView(R.id.item_ambiente_qtd_dispositivos)
        TextView quantidadeDipositivos;
        @BindView(R.id.item_ambiente)
        CardView cardViewAmbiente;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cardViewAmbiente.setOnCreateContextMenuListener(this);
        }

        @OnClick(R.id.item_ambiente)
        public void onClick() {
            Ambiente ambiente = new AmbienteDao(context).findById(Long.valueOf(id.getText().toString()));
            Intent intent = new Intent(context, ListarDispositivoActivity.class);
            intent.putExtra("AMBIENTE", ambiente);
            context.startActivity(intent);
        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            contextMenu.add(this.getAdapterPosition(), Tools.UPDATE, 0, "Editar");
            contextMenu.add(this.getAdapterPosition(), Tools.DELETE, 1, "Deletar");
            //contextMenu.add(this.getAdapterPosition(), 3, 2, "Ver Imagem");
        }
    }

    public void deleteAmbiente(long id) {
        new AmbienteDao(context).delete(id);
        notifyDataSetChanged();
    }


}
