package com.belarmino.scenarioautomation.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.activity.ListarAmbienteActivity;
import com.belarmino.scenarioautomation.dao.ProjetoDao;
import com.belarmino.scenarioautomation.model.Projeto;
import com.belarmino.scenarioautomation.util.Tools;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjetoAdapter extends RecyclerView.Adapter{

    private Context context;
    private List<Projeto> projetos;

    public ProjetoAdapter(Context context, List<Projeto> projetos) {
        this.context = context;
        this.projetos = projetos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_projeto, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ViewHolder holder = (ViewHolder) viewHolder;
        Projeto projeto = projetos.get(position);

        holder.id.setText(""+projeto.getId());
        holder.nomeProjeto.setText(projeto.getNomeProjeto());
        holder.nomeCliente.setText(projeto.getNomeCliente());
        holder.endereco.setText(projeto.getEndereco());
        holder.data.setText(Tools.dateFormat(projeto.getDataAtualizacao(),"dd-MM-yyyy"));
    }

    @Override
    public int getItemCount() {
        return projetos.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        @BindView(R.id.item_projeto_id)
        TextView id;
        @BindView(R.id.item_projeto_nome_projeto)
        TextView nomeProjeto;
        @BindView(R.id.item_projeto_nome_cliente)
        TextView nomeCliente;
        @BindView(R.id.item_projeto_endereco)
        TextView endereco;
        @BindView(R.id.item_projeto_data)
        TextView data;
        @BindView(R.id.item_projeto)
        CardView cardViewProjeto;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            cardViewProjeto.setOnCreateContextMenuListener(this);
        }

        @OnClick(R.id.item_projeto)
        public void onClick() {
            projetos = new ProjetoDao(context).findAll();
            Projeto projeto = projetos.get(getAdapterPosition());
            Intent intent = new Intent(context, ListarAmbienteActivity.class);
            intent.putExtra("PROJETO", projeto);
            context.startActivity(intent);
        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

            contextMenu.add(this.getAdapterPosition(), Tools.UPDATE, 0, "Editar");
            contextMenu.add(this.getAdapterPosition(), Tools.DELETE, 1, "Deletar");
        }
    }

    public void deleteProjeto(long id) {
        new ProjetoDao(context).delete(id);
        notifyDataSetChanged();
    }
}
