package com.belarmino.scenarioautomation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.dao.ProjetoDao;
import com.belarmino.scenarioautomation.model.Projeto;

import java.util.Date;

public class ProjetoActivity extends AppCompatActivity {

    EditText etNomeProjeto;
    EditText etNomeCliente;
    EditText etEndereco;
    Projeto projeto = null;
    private boolean novo = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projeto);


        projeto = (Projeto) getIntent().getSerializableExtra("PROJETO");
        etNomeProjeto = (EditText) findViewById(R.id.nomeProjeto);
        etNomeCliente = (EditText) findViewById(R.id.nomeCliente);
        etEndereco = (EditText) findViewById(R.id.endereco);

        if (projeto != null) {
            etNomeCliente.setText(projeto.getNomeCliente());
            etNomeProjeto.setText(projeto.getNomeProjeto());
            etEndereco.setText(projeto.getEndereco());
            novo = false;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_projeto, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (novo) {
            projeto = new Projeto();
        }
        String nomeProjeto = etNomeProjeto.getText().toString().trim();
        String nomecliente = etNomeCliente.getText().toString().trim();
        String endereco = etEndereco.getText().toString().trim();


        switch (item.getItemId()) {
            case R.id.save:
                if (nomeProjeto.isEmpty()) {
                    etNomeProjeto.setError("Informe um nome para o Projeto!");
                } else if (nomecliente.isEmpty()) {
                    etNomeCliente.setError("Informe o nome do Cliente!");
                } else if (endereco.isEmpty()) {
                    etEndereco.setError("Informe o Endereço do Projeto!");
                }else {
                    projeto.setNomeCliente(nomecliente);
                    projeto.setNomeProjeto(nomeProjeto);
                    projeto.setEndereco(endereco);
                    projeto.setDataAtualizacao(new Date());

                    ProjetoDao projetoDao = new ProjetoDao(this);
                    long id = 0;
                    if (novo) {
                        id = projetoDao.insert(projeto);
                    } else {
                        id = projetoDao.update(projeto);
                    }
                    projetoDao.close();
                    if(id>0){
                        Toast.makeText(this, "Sucesso", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(this, MainActivity.class));
                        finish();
                    }else{
                        Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
                    }

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
