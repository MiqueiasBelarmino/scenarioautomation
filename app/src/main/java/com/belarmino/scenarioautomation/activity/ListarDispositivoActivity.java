package com.belarmino.scenarioautomation.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.adapter.DispositivoAdapter;
import com.belarmino.scenarioautomation.dao.DispositivoComumDao;
import com.belarmino.scenarioautomation.dao.DispositivoScenarioDao;
import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.model.DispositivoComum;
import com.belarmino.scenarioautomation.model.DispositivoScenario;
import com.belarmino.scenarioautomation.util.Tools;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListarDispositivoActivity extends AppCompatActivity {

    @BindView(R.id.recycler_listar_dispositivo_activity)
    RecyclerView recyclerView;
    private FloatingActionButton fab;
    private DispositivoAdapter dispositivoAdapter;
    private Ambiente ambiente = null;
    private List<Object> objs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_dispositivo);

        ButterKnife.bind(this);
        ambiente = (Ambiente) getIntent().getSerializableExtra("AMBIENTE");

        if (ambiente != null) {
            objs.addAll(new DispositivoScenarioDao(this).findByAmbiente(ambiente.getId()));
            objs.addAll(new DispositivoComumDao(this).findByAmbiente(ambiente.getId()));
        }

        dispositivoAdapter = new DispositivoAdapter(this, objs);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(dispositivoAdapter);

        fab = findViewById(R.id.add);
        fab.setOnClickListener(view ->
                clickFloatingActionButton()
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        recarregar();

    }

    private void clickFloatingActionButton() {
        startActivity(new Intent(this, DispositivoActivity.class).putExtra("AMBIENTE", ambiente));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        String nome = "Sem nome";

        switch (item.getItemId()) {
            case Tools.UPDATE:
                if (objs.get(item.getGroupId()) instanceof DispositivoScenario) {
                    DispositivoScenario ds = (DispositivoScenario) objs.get(item.getGroupId());
                    Intent intent = new Intent(this, DispositivoActivity.class);
                    intent.putExtra("SCENARIO", ds);
                    startActivity(intent);
                    finish();
                } else {
                    DispositivoComum dc = (DispositivoComum) objs.get(item.getGroupId());
                    Intent intent = new Intent(this, DispositivoActivity.class);
                    intent.putExtra("COMUM", dc);
                    startActivity(intent);
                    finish();
                }
                return true;
            case Tools.DELETE:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Atenção")
                        .setMessage("Deseja realmente deletar o Dispositivo?")
                        .setNegativeButton("Não", null)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dispositivoAdapter.deleteDispositivo(objs.get(item.getGroupId()));
                                recarregar();
                            }
                        }).create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void recarregar() {
        objs.clear();
        objs.addAll(new DispositivoScenarioDao(this).findByAmbiente(ambiente.getId()));
        objs.addAll(new DispositivoComumDao(this).findByAmbiente(ambiente.getId()));
        recyclerView.setAdapter(new DispositivoAdapter(this, objs));
    }

}
