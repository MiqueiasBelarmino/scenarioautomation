package com.belarmino.scenarioautomation.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.adapter.AmbienteAdapter;
import com.belarmino.scenarioautomation.dao.AmbienteDao;
import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.model.Projeto;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListarAmbienteActivity extends AppCompatActivity {

    @BindView(R.id.recycler_listar_ambiente_activity)
    RecyclerView recyclerView;
    private FloatingActionButton fab;
    private Projeto projeto = null;
    private List<Ambiente> ambientes;
    private AmbienteAdapter ambienteAdapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_ambiente);

        ButterKnife.bind(this);

        projeto = (Projeto) getIntent().getSerializableExtra("PROJETO");
        ambientes = new AmbienteDao(this).findByProjeto(projeto.getId());
        ambienteAdapter = new AmbienteAdapter(this, ambientes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(ambienteAdapter);

        fab = findViewById(R.id.add);
        fab.setOnClickListener(view ->
                clickFloatingActionButton()
        );
    }

    private void clickFloatingActionButton() {
        startActivity(new Intent(this, AmbienteActivity.class).putExtra("PROJETO", projeto));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        recarregar();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Intent intent = new Intent(this, AmbienteActivity.class);
                intent.putExtra("AMBIENTE", ambientes.get(item.getGroupId()));
                startActivity(intent);
                finish();
                return true;
            case 2:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Atenção")
                        .setMessage("Deseja realmente deletar o Ambiente?\nDispositivos do ambiente também serão deletados!")
                        .setNegativeButton("Não", null)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ambienteAdapter.deleteAmbiente(ambientes.get(item.getGroupId()).getId());
                                recarregar();
                            }
                        }).create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void recarregar() {
        AmbienteDao dao = new AmbienteDao(this);
        ambientes = dao.findByProjeto(projeto.getId());
        dao.close();

        recyclerView.setAdapter(new AmbienteAdapter(this, ambientes));
    }

}
