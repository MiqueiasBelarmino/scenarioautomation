package com.belarmino.scenarioautomation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.dao.DadosDispositivoDao;
import com.belarmino.scenarioautomation.dao.DispositivoComumDao;
import com.belarmino.scenarioautomation.dao.DispositivoScenarioDao;
import com.belarmino.scenarioautomation.dao.TiposDispositivoDao;
import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.model.DadosDispositivo;
import com.belarmino.scenarioautomation.model.DispositivoComum;
import com.belarmino.scenarioautomation.model.DispositivoScenario;
import com.belarmino.scenarioautomation.model.TiposDispositivo;

import java.util.ArrayList;

public class DispositivoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Ambiente ambiente = null;
    private DispositivoScenario dispositivoScenario = null;
    private DispositivoComum dispositivoComum = null;
    private DadosDispositivo dadosDispositivo = null;
    private ArrayList<TiposDispositivo> tipos;
    private boolean novo = true;
    private boolean scenario = true;
    Spinner spinnerTipoScenario;
    EditText etNome;
    EditText etFabricante;
    TextView tvTipo;
    RadioGroup radioGroup;
    RadioButton radioDispositivo;
    RadioButton radioScenario;
    RadioButton radioComum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivo);

        spinnerTipoScenario = (Spinner) findViewById(R.id.spinner_tipo_scenario);
        etFabricante = (EditText) findViewById(R.id.dispositivo_fabricante);
        etNome = (EditText) findViewById(R.id.dispositivo_nome);
        tvTipo = (TextView) findViewById(R.id.tv_tipo);
        radioComum = (RadioButton) findViewById(R.id.rb_comum);
        radioScenario = (RadioButton) findViewById(R.id.rb_scenario);
        etFabricante.setVisibility(View.GONE);

        addSpinnerAdapter();
        addListenerRadioGroup();

        ambiente = (Ambiente) getIntent().getSerializableExtra("AMBIENTE");
        dispositivoScenario = (DispositivoScenario) getIntent().getSerializableExtra("SCENARIO");
        dispositivoComum = (DispositivoComum) getIntent().getSerializableExtra("COMUM");

        if (dispositivoScenario != null) {

            radioScenario.setChecked(true);
            etNome.setText(dispositivoScenario.getDadosDispositivo().getNome());
            spinnerTipoScenario.setSelection(posicaoTipoDispositivo(tipos, dispositivoScenario.getTipo().getDescricao()));
            ambiente = dispositivoScenario.getAmbiente();
            dadosDispositivo = dispositivoScenario.getDadosDispositivo();

            novo = false;

        } else if (dispositivoComum != null) {

            radioComum.setChecked(true);
            etNome.setText(dispositivoComum.getDadosDispositivo().getNome());
            etFabricante.setText(dispositivoComum.getFabricante());
            ambiente = dispositivoComum.getAmbiente();
            dadosDispositivo = dispositivoComum.getDadosDispositivo();

            novo = false;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nome = etNome.getText().toString().trim();
        String fabricante = etFabricante.getText().toString().trim();
        long dadosId;

        if (novo) {
            dadosDispositivo = new DadosDispositivo();
            dispositivoScenario = new DispositivoScenario();
            dispositivoComum = new DispositivoComum();
        }

        switch (item.getItemId()) {
            case R.id.save:
                if (nome.isEmpty()) {
                    etNome.setError("Informe o nome do Dispositivo!");
                } else {
                    dadosDispositivo.setNome(nome);
                    dadosId = new DadosDispositivoDao(this).insert(dadosDispositivo);

                    if (scenario) {
                        salvarDispositivoScenario(dadosId);
                    } else {
                        if (fabricante.isEmpty()) {
                            etFabricante.setError("Informe o Fabricante!");
                        } else {
                            salvarDispositivoComum(dadosId);
                        }
                    }
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private int posicaoTipoDispositivo(ArrayList<TiposDispositivo> arrayList, String descricao) {

        int i = 0;
        for (; i < arrayList.size(); i++) {
            if (arrayList.get(i).getDescricao().equals(descricao)) {
                return i;
            }
        }
        return i;
    }

    private void salvarDispositivoScenario(long dadosId) {

        dispositivoScenario.setAmbiente(ambiente);
        dispositivoScenario.setDadosDispositivo(new DadosDispositivoDao(this).findById(dadosId));
        dispositivoScenario.setTipo((TiposDispositivo) spinnerTipoScenario.getSelectedItem());

        DispositivoScenarioDao scenarioDao = new DispositivoScenarioDao(this);
        long id = 0;
        if (novo) {
            id = scenarioDao.insert(dispositivoScenario);
        } else {
            id = scenarioDao.update(dispositivoScenario);
        }
        scenarioDao.close();
        if (id > 0) {
            Toast.makeText(this, "Sucesso", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, ListarDispositivoActivity.class).putExtra("AMBIENTE", ambiente));
            finish();
        } else {
            Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
        }
    }

    private void salvarDispositivoComum(long dadosId) {

        String fabricante = etFabricante.getText().toString().trim();
        dispositivoComum.setAmbiente(ambiente);
        dispositivoComum.setDadosDispositivo(new DadosDispositivoDao(this).findById(dadosId));
        dispositivoComum.setFabricante(fabricante);

        DispositivoComumDao comumDao = new DispositivoComumDao(this);
        long id = 0;
        if (novo) {
            id = comumDao.insert(dispositivoComum);
        } else {
            id = comumDao.update(dispositivoComum);
        }
        comumDao.close();
        if (id > 0) {
            Toast.makeText(this, "Sucesso", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, ListarDispositivoActivity.class).putExtra("AMBIENTE", ambiente));
            finish();
        } else {
            Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
        }
    }

    private void addSpinnerAdapter() {
        tipos = (ArrayList<TiposDispositivo>) new TiposDispositivoDao(this).findAll();
        ArrayAdapter<TiposDispositivo> adapterTipo = new ArrayAdapter<TiposDispositivo>(this, android.R.layout.simple_spinner_item, tipos);
        adapterTipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoScenario.setAdapter(adapterTipo);
        spinnerTipoScenario.setOnItemSelectedListener(this);
    }

    private void addListenerRadioGroup() {

        radioGroup = (RadioGroup) findViewById(R.id.radio_grupo_dispositivos);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioDispositivo = (RadioButton) group.findViewById(checkedId);
                String txt = radioDispositivo.getText().toString();
                if (txt.equals("Comum")) {
                    spinnerTipoScenario.setVisibility(View.GONE);
                    tvTipo.setVisibility(View.GONE);
                    etFabricante.setVisibility(View.VISIBLE);
                    scenario = false;
                } else {
                    spinnerTipoScenario.setVisibility(View.VISIBLE);
                    tvTipo.setVisibility(View.VISIBLE);
                    etFabricante.setVisibility(View.GONE);
                    scenario = true;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_projeto, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
