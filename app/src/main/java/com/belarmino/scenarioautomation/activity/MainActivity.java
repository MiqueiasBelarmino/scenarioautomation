package com.belarmino.scenarioautomation.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.adapter.ProjetoAdapter;
import com.belarmino.scenarioautomation.dao.DBHelper;
import com.belarmino.scenarioautomation.dao.ProjetoDao;
import com.belarmino.scenarioautomation.model.Projeto;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler_projetos_activity)
    RecyclerView recyclerView;
    private FloatingActionButton fab;
    private List<Projeto> projetos;
    private ProjetoAdapter projetoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle("Meus Projetos");
        ButterKnife.bind(this);

        projetos = new ProjetoDao(this).findAll();
        projetoAdapter = new ProjetoAdapter(this, projetos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(projetoAdapter);
        DBHelper dbHelper = new DBHelper(this);

        fab = findViewById(R.id.add);
        fab.setOnClickListener(view ->
                clickFloatingActionButton()
        );
    }

    private void clickFloatingActionButton() {
        startActivity(new Intent(this, ProjetoActivity.class));
        finish();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Intent intent = new Intent(this, ProjetoActivity.class);
                intent.putExtra("PROJETO", projetos.get(item.getGroupId()));
                startActivity(intent);
                return true;
            case 2:
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Atenção")
                        .setMessage("Deseja realmente deletar o Projeto?\nAmbientes e Dispositivos do projeto também serão deletados!")
                        .setNegativeButton("Não", null)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                projetoAdapter.deleteProjeto(projetos.get(item.getGroupId()).getId());
                                recarregar();
                            }
                        }).create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void recarregar() {
        ProjetoDao dao = new ProjetoDao(this);
        projetos = dao.findAll();
        dao.close();

        recyclerView.setAdapter(new ProjetoAdapter(this, projetos));
    }

    @Override
    protected void onResume() {
        super.onResume();

        recarregar();

    }
}
