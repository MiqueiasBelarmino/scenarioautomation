package com.belarmino.scenarioautomation.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.belarmino.scenarioautomation.R;
import com.belarmino.scenarioautomation.dao.AmbienteDao;
import com.belarmino.scenarioautomation.dao.ProjetoDao;
import com.belarmino.scenarioautomation.model.Ambiente;
import com.belarmino.scenarioautomation.model.Projeto;
import com.belarmino.scenarioautomation.util.Tools;

public class AmbienteActivity extends AppCompatActivity {

    private ImageView selectedImageView;
    private Projeto projeto = null;
    private Ambiente ambiente = null;
    private boolean novo = true;
    private static final int REQUEST_CODE_GALLERY = 999;
    private static final int CAMERA_REQUEST_CODE = 200;

    TextView etNomeAmbiente;
    TextView tvProjeto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambiente);

        selectedImageView = (ImageView) findViewById(R.id.imageView);
        tvProjeto = (TextView) findViewById(R.id.nome_projeto);
        etNomeAmbiente = (TextView) findViewById(R.id.nome_ambiente);

        ambiente = (Ambiente) getIntent().getSerializableExtra("AMBIENTE");

        if (ambiente != null) {
            projeto = new ProjetoDao(this).findById(ambiente.getProjeto().getId());
            novo = false;
            etNomeAmbiente.setText(ambiente.getNome());
            Bitmap bitmap = BitmapFactory.decodeByteArray(ambiente.getImagem(), 0, ambiente.getImagem().length);
            selectedImageView.setImageBitmap(bitmap);

        } else {
            projeto = (Projeto) getIntent().getSerializableExtra("PROJETO");
            novo = true;
        }

        tvProjeto.setText("Projeto: " + projeto.getNomeProjeto());


        selectedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(AmbienteActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(this, "Permissão Negada", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            selectedImageView.setImageURI(imageUri);
        }

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            selectedImageView.setImageBitmap(imageBitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_projeto, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void openGallery(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Selecione uma imagem"), REQUEST_CODE_GALLERY);
    }

    public void openCamera(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (novo) {
            ambiente = new Ambiente();
        }
        String nome = etNomeAmbiente.getText().toString().trim();

        switch (item.getItemId()) {

            case R.id.save:
                if (nome.isEmpty()) {
                    etNomeAmbiente.setError("Informe um nome para o Ambiente!");
                } else {
                    ambiente.setProjeto(projeto);
                    ambiente.setImagem(Tools.imageViewToByte(selectedImageView));
                    ambiente.setNome(nome);

                    AmbienteDao ambienteDao = new AmbienteDao(this);
                    long id = 0;
                    if (novo) {
                        id = ambienteDao.insert(ambiente);
                    } else {
                        id = ambienteDao.update(ambiente);
                    }
                    ambienteDao.close();
                    if (id > 0) {
                        Toast.makeText(this, "Sucesso", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(this, ListarAmbienteActivity.class).putExtra("PROJETO", projeto));
                        finish();
                    } else {
                        Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
