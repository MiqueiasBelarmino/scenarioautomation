package com.belarmino.scenarioautomation.model;

import java.io.Serializable;

public class DadosDispositivo implements Serializable {

    private long id;
    private String nome;

    public DadosDispositivo() {
    }

    public DadosDispositivo(long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
