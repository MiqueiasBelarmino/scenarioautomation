package com.belarmino.scenarioautomation.model;

import java.io.Serializable;

public class DispositivoScenario implements Serializable {

    private long id;
    private TiposDispositivo tipo;
    private Ambiente ambiente;
    private DadosDispositivo dadosDispositivo;


    public DispositivoScenario() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TiposDispositivo getTipo() {
        return tipo;
    }

    public void setTipo(TiposDispositivo tipo) {
        this.tipo = tipo;
    }

    public Ambiente getAmbiente() {
        return ambiente;
    }
    public void setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
    }

    public DadosDispositivo getDadosDispositivo() {
        return dadosDispositivo;
    }

    public void setDadosDispositivo(DadosDispositivo dadosDispositivo) {
        this.dadosDispositivo = dadosDispositivo;
    }

}
