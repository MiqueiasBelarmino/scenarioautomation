package com.belarmino.scenarioautomation.model;

import java.io.Serializable;

public class DispositivoComum implements Serializable {

    private long id;
    private String fabricante;
    private Ambiente ambiente;
    private DadosDispositivo dadosDispositivo;


    public DispositivoComum() {
    }


    public DispositivoComum(String fabricante) {
        this.fabricante = fabricante;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public Ambiente getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
    }

    public DadosDispositivo getDadosDispositivo() {
        return dadosDispositivo;
    }

    public void setDadosDispositivo(DadosDispositivo dadosDispositivo) {
        this.dadosDispositivo = dadosDispositivo;
    }
}
